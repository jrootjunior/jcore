from distutils.core import setup

setup(
    name='jcore_framework',
    version='0.0.1',
    packages=['jcore'],
    url='https://bitbucket.org/jrootjunior/jcore/',
    license='GNU GPL v3',
    author='Alex Root Junior',
    author_email='jroot.junior@gmail.com',
    description='JCore Framework'
)
