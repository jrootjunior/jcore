"""
    JCore Framework. Crash handler
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jcore.utils import *
import traceback as tb

# FIXME: It's not perfect hook. Not always write to file. But.. Why?!?


class CrashHandler:
    """
    Handle crashing and write to file
    """

    def __init__(self, jcore):
        """
        Override default except hook
        :param jcore: JCore object
        """
        sys.old_excepthook = sys.excepthook
        self.jcore = jcore
        self.setup()

    def _except_hook(self, exception_type, exception_value, traceback):
        # Output to screen
        tb.print_exception(exception_type, exception_value, traceback)

        # Generate filename
        filename = CRASHREPORT_LOCATION + '/crashreport_{datetime}.txt'.format(
            datetime=time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())
        )
        self.jcore.log_core.error('Generated crash report: ' + filename)

        # Check crash reports directory
        if not os.path.exists(CRASHREPORT_LOCATION):
            os.mkdir(CRASHREPORT_LOCATION)

        # Write report
        with open(filename, 'w') as file:
            file.write('Date: ' + time.strftime("%d.%m.%Y", time.localtime()) + '\n')
            file.write('Time: ' + time.strftime("%H:%M:%S", time.localtime()) + '\n')
            file.write('Session: ' + self.jcore.get_session_key() + '\n\n')
            file.write('\n'.join(tb.format_exception(exception_type, exception_value, traceback)))

    def setup(self):
        sys.excepthook = self._except_hook