"""
    JCore Framework.
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jcore.crash_handler import CrashHandler
from jcore.watchdog import WatchDog
from jcore.funshaker import FunShaker
from jcore.config import Config
from jcore.logging import CoreLog
from jcore.utils import *
from jcore.version import *


class JCore:
    """
    Main object
    """
    __version__ = version
    __author__ = contributor
    __license__ = license_text

    def __init__(self):
        self.time_startup = int(time.time())
        # Generate session key
        self.session_key = gen_key()

        self.log_core = CoreLog('core', logging.DEBUG, self.session_key)
        self.crash = CrashHandler(self)

        # Setup loggers
        self.log_loader = CoreLog('loader', logging.DEBUG, self.session_key)
        self.log = CoreLog('main-frame', logging.DEBUG, self.session_key)
        self.log_plugins = CoreLog('plugins', logging.DEBUG, self.session_key)
        self.log_fun = CoreLog('fun', logging.DEBUG, self.session_key)

        # Load config
        self.config = Config('config', DEFAULT_CONFIG, auto_load=True, logger=self.log_loader)
        self.cache = Config('cache', logger=self.log_loader)

        # Load modules
        self.watchdog = WatchDog(self)
        self.fun = FunShaker(self)
        self.rdb = rethinkdb.connect(host=self.config.data['rethinkdb']['host'],
                                     port=self.config.data['rethinkdb']['port'],
                                     db=self.config.data['rethinkdb']['db'],
                                     auth_key=self.config.data['rethinkdb']['auth_key'])

        # Initial output
        self.time_loaded = int(time.time())
        self.log_core.system('Inited JCore v:' + self.__version__)

    def get_session_key(self):
        return self.session_key

    def get_app_location(self):
        return APP_LOCATION
