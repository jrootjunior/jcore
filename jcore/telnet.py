"""
    JCore Framework. Telnet module
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import socket
import threading
import time
import rethinkdb as r
from jcore.logging import CoreLog
from jcore.utils import GPerm
from jcore.watchdog import WSignal

JCMD_VERSION = '0.2 (Special. Telnet)'
RDB_TABLE_USERS = 'telnet_users'


class Telnet:
    clients = []
    _commands = []
    input_title = 'jcore'

    def __init__(self, jcore, address='0.0.0.0', port=23579, max_connection=1, recv=1024):
        self.jcore = jcore
        self.address = address
        self.port = port
        self.max_connection = max_connection
        self.recv = recv

        # Register logger
        self.log = CoreLog('telnet', logging.DEBUG, jcore.session_key)

        # Setup socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.setblocking(False)
        self._socket.bind((address, port))
        self._socket.listen(max_connection)

        # Start main routine
        self.jcore.watchdog.add_watch(self._process, 'Socket', True)

        # Register telnet commands
        self.add_command(['help', '?'], self.cmd_help, "Show commands list")
        self.add_command(['exit', 'close'], self.cmd_exit, "Close connection")

        # Setup rethink db
        self.rdb = r.db(self.jcore.config.data['rethinkdb']['db'])
        tables = self.rdb.table_list().run(self.jcore.rdb)
        if RDB_TABLE_USERS not in tables:
            self.rdb.table_create(RDB_TABLE_USERS).run(self.jcore.rdb)
            self.user_register('root', '', 0)
        self.telnet_users = self.rdb.table(RDB_TABLE_USERS)

    def _process(self):
        # Main process. Provided by WatchDog
        self.log.info('Start listening clients on: ' + self.address + ':' + str(self.port))

        def routine(title, signals):
            # Listen clients
            try:
                (clientsock, (ip, port)) = self._socket.accept()
            except BlockingIOError:
                return True
            except OSError as e:
                self.log.error(e)
                return False

            # Execute client thread
            client = ClientThread(self, ip, port, clientsock, self.recv)
            client.start()
            self.clients.append(client)
            return True

        try:
            self.jcore.watchdog.loop('Socket', routine, 0)
        except KeyboardInterrupt:
            self.jcore.watchdog.signal('KeyboardInterrupt', WSignal.SIGSTOP)

    def close_all(self):
        self.log.warning('Close connections.')

        # Close all connections and destroy user objects.
        for client in self.clients:
            client.send('\nYou are kicked from server!\n')
            try:
                client.socket.shutdown(socket.SHUT_RDWR)
            except OSError:
                pass
            finally:
                client.socket.close()
            del client

        # Stop socket server
        try:
            self._socket.shutdown(socket.SHUT_RDWR)
        except OSError:
            pass
        self._socket.close()

    def handler(self, client, data):
        # Console handler
        params = data.split(' ')
        header = params[0]

        for item in self._commands:
            if header.lower() in item['command']:
                func = item['function']
                result = func(client, data)
                if result:
                    client.send(result)
                    return False
                else:
                    return False
        client.send('JCMD: Command "{cmd}" not found!\n      See "help"\n'.format(cmd=header))

    def add_command(self, command, function, description):
        reg = {'command': command, 'function': function, 'description': description}
        self._commands.append(reg)
        return reg

    def del_command(self, data):
        if type(data).__name__ == 'list':
            for command in data:
                self.del_command(command)
        else:
            if data in self._commands:
                self._commands.pop(self._commands.index(data))

    def get_commands(self):
        return self._commands

    def get_clients(self):
        return self.clients

    def gen_data(self, text):
        return bytes(text, 'utf-8')

    def set_input_header(self, title):
        self.input_title = title

    def send_all(self, message, auth=True, level=GPerm.ANONYMOUS):
        for clientthread in self.clients:
            if auth and len(clientthread.login) and GPerm.check(clientthread.level, level):
                clientthread.send('\n' + message + '\n')
            elif not auth:
                clientthread.send('\n' + message + '\n')

    def user_register(self, login, password, permission=GPerm.WHITELISTED):
        for user in self.rdb.table(RDB_TABLE_USERS).run(self.jcore.rdb):
            if login == user['login']:
                return 'User already registered!'

        queue = {
            'login': login,
            'password': password,
            'permission': permission,
            'registered': int(time.time()),
            'lastlogin': {
                'ip': '',
                'port': '',
                'time': -1
            },
            'email': '',
            'blocked': False
        }
        self.rdb.table(RDB_TABLE_USERS).insert(queue).run(self.jcore.rdb)
        self.log.info('Registered new user account - "' + login + '"')
        return 'Registered new user account - "' + login + '"'

    def cmd_exit(self, client, data):
        """
    Close socket connection.
        """
        client.send('\n\tBye!\n')
        client.socket.shutdown(socket.SHUT_RDWR)
        client.socket.close()
        return False

    def cmd_help(self, client, data):
        """
    These shell commands are defined internally.  Type 'help' to see this list.
    Type 'help name' to find out more about the function 'name'.
        """
        if len(data.split()) > 1:
            ask_cmd = data.split()[1]
            for item in self._commands:
                if ask_cmd in self._commands[self._commands.index(item)]['command']:
                    func = self._commands[self._commands.index(item)]['function']
                    if func.__doc__ is not None:
                        return 'Help for \'{cmd}\':{info}\n'.format(cmd=ask_cmd, info=func.__doc__)
                    else:
                        return 'JCMD: No manual entry for import.\n'

        items = ['JCMD version {ver}'.format(ver=JCMD_VERSION),
                 'These shell commands are defined internally.  Type \'help\' to see this list.',
                 '']
        for item in self._commands:
            items.append('\t{cmd} - {description}.'
                         .format(cmd=', '.join(item['command']),
                                 description=item['description']))
        return '\n'.join(items) + '\n'


class ClientThread(threading.Thread):
    def __init__(self, telnet, ip, port, socket, recv=1024):
        threading.Thread.__init__(self)
        self.name = 'Client(' + ip + ':' + str(port) + ')'
        self.telnet = telnet
        self.ip = ip
        self.port = port
        self.socket = socket
        self.recv = recv
        self.telnet.log.info('Connected: ' + self.get_address())

        self.ps1 = self.telnet.input_title
        self.login = ''
        self.username = ''
        self.email = ''
        self.level = GPerm.ANONYMOUS
        self.uid = ''
        self.lastlogin = {}

    def run(self):
        data = "1"
        # Welcome connected user
        self.send('\nWelcome Anonymous user (' + self.get_address() + ')')
        self.send('\nTime on server: ' + time.ctime(int(time.time())) + '')
        self.send('\nSession: ' + self.telnet.jcore.session_key + '\n')

        # Try auth
        if not self.auth():
            return False

        # IO Reader
        # FIXME: Closing connection if client sent empty line (Assign report: #1: Telnet: Lost connection.)
        while len(data):
            # self.send('\n' + self.ps1 + '> ')
            data = self.read(self.recv)
            if len(data):
                self.telnet.log.info('Client (' + self.get_address() + ' ' + self.login + ' ' + self.uid +
                                     ') sent : ' + data)
                self.telnet.handler(self, data)
            else:
                self.socket.close()
                break
        self.telnet.log.info('Disconnected: ' + self.get_address())
        self.telnet.clients.remove(self)

    def auth(self):
        """
        Main authorization function.
        Provide loading users from RDB
        :return: bool, auth status.
        """
        # Get login/password
        self.send('\nLogin: ')
        login = self.read(1024)
        self.send('Password: ')
        password = self.read(1024)

        # Scan users table
        users = self.telnet.telnet_users.filter(r.row["login"] == login.lower()).run(self.telnet.jcore.rdb)
        for user in users:
            # Check password
            if user['password'] == password:
                # Get user data
                self.uid = user['id']
                self.login = user['login']
                self.email = user['email']
                self.level = user['permission']
                self.lastlogin = user['lastlogin']
                self.set_ps1(self.login + '@' + self.telnet.input_title)

                # Welcome connected user
                self.send('\nWelcome back, ')
                if 'name' not in user:
                    self.send(user['login'] + '!\n')
                else:
                    self.username = user['name']
                    self.send(user['name'] + '!\n')

                self.telnet.log.info('User ' + self.get_address() + ' logged in as ' + self.login + ' (' + \
                                     ['noname', self.username][len(self.username) > 0] + ') ' + \
                                     GPerm.get_hname(self.level).upper() + ' ' + \
                                     self.uid)

                # Show last login information
                self.send('Last login: ')
                if user['lastlogin']['time'] < 0:
                    self.send('it\'s first.')
                else:
                    self.send(time.ctime(user['lastlogin']['time']))
                    self.send(' from ' + user['lastlogin']['ip'] + ':' + str(user['lastlogin']['port']) + '\n')

                # Update last login information in database
                self.telnet.telnet_users.get(user['id']).update({
                    'lastlogin': {
                        'ip': self.ip,
                        'port': self.port,
                        'time': time.time()
                    }
                }).run(self.telnet.jcore.rdb)
                # Done!
                return True

        # If user not found, close connection.
        self.send('\nWrong login or password! :(\n')
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        # Done!
        return False

    def send(self, message):
        try:
            self.socket.send(bytes(message, 'utf-8'))
        except:
            pass

    def read(self, len):
        try:
            return self.socket.recv(len).decode('utf-8')[:-2]
        except:
            return ''

    def set_ps1(self, text):
        self.ps1 = text

    def get_address(self):
        return self.ip + ':' + str(self.port)
