"""
    JCore Framework.
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import random
import sys
import time
import json
import logging
import threading
import rethinkdb

APP_LOCATION = os.path.dirname(os.path.realpath(sys.argv[0]))
DATA_LOCATION = APP_LOCATION + '/data'
PLUGINS_LOCATION = APP_LOCATION + '/plugins'
CRASHREPORT_LOCATION = APP_LOCATION + '/crash'
BACKUPS_LOCATION = APP_LOCATION + '/backup'

DEFAULT_CONFIG = {
    'debug': True,
    'name': 'Kittie',
    'fun': {
        'timeout': 2
    },
    'rethinkdb': {
        'host': 'localhost',
        'port': 28015,
        'db': 'jcore',
        'auth_key': ''
    }
}

GEN_LOWER = 'abcdefghijklmnopqrstuvwxyz'
GEN_UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
GEN_NUMBERS = '0123456789'
GEN_SYMBOLS = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'


class GPerm(object):
    ROOT = 0
    ADMINISTRATOR = 10
    DONATOR = 20
    MODERATOR = 50
    LISTENER = 100
    WHITELISTED = 500
    ANONYMOUS = 1000

    _id = {
        ROOT: 'root',
        ADMINISTRATOR: 'administrator',
        DONATOR: 'donator',
        MODERATOR: 'moderator',
        LISTENER: 'listener',
        WHITELISTED: 'whitelisted',
        ANONYMOUS: 'anonymous'
    }

    _name = {
        'root': ROOT,
        'administrator': ADMINISTRATOR,
        'donator': DONATOR,
        'moderator': MODERATOR,
        'listener': LISTENER,
        'whitelisted': WHITELISTED,
        'anonymous': ANONYMOUS
    }

    @classmethod
    def get_name(cls, level):
        if level in cls._id:
            return cls._id[level]
        return cls._id[cls.ANONYMOUS]

    @classmethod
    def get_hname(cls, level):
        if level == cls.ROOT:
            return cls.get_name(cls.ROOT)
        elif level <= cls.ADMINISTRATOR:
            return cls.get_name(cls.ADMINISTRATOR)
        elif level <= cls.DONATOR:
            return cls.get_name(cls.DONATOR)
        elif level <= cls.MODERATOR:
            return cls.get_name(cls.MODERATOR)
        elif level <= cls.LISTENER:
            return cls.get_name(cls.LISTENER)
        elif level <= cls.WHITELISTED:
            return cls.get_name(cls.WHITELISTED)
        else:
            return cls.get_name(cls.ANONYMOUS)

    @classmethod
    def get_level(cls, name):
        if name in cls._name:
            return cls._name[name]
        else:
            return cls._name['anonymous']

    @classmethod
    def get_list(cls):
        result = []
        for item in cls._id:
            result.append(item)
        result.sort()
        for item in result:
            result[result.index(item)] = cls.get_name(item)
        return result

    @classmethod
    def check(cls, user, need):
        if user <= need:
            return True
        return False


def gen_random_word(length, lower=True, upper=False, numbers=True, symbols=False):
    preset = ''
    if lower:
        preset += GEN_LOWER
    if upper:
        preset += GEN_UPPER
    if numbers:
        preset += GEN_NUMBERS
    if symbols:
        preset += GEN_SYMBOLS
    if len(preset) > 0:
        result = ''
        for pos in range(length):
            result += random.choice(preset)
        return result
    else:
        return ''


def valid_json(string_data):
    try:
        json_object = json.loads(string_data)
    except ValueError as e:
        return None
    return json_object


def gen_key(upper=True):
    """
    Generate key. 8-4-4-4-12
    Sample: 141f4503-1788-4695-9369-31546a735dc4
    :return:
    """
    return gen_random_word(8, lower=not upper, upper=upper, numbers=True, symbols=False) + '-' + \
        gen_random_word(4, lower=not upper, upper=upper, numbers=True, symbols=False) + '-' + \
        gen_random_word(4, lower=not upper, upper=upper, numbers=True, symbols=False) + '-' + \
        gen_random_word(4, lower=not upper, upper=upper, numbers=True, symbols=False) + '-' + \
        gen_random_word(12, lower=not upper, upper=upper, numbers=True, symbols=False)


def delete_module(modname, paranoid=None):
    from sys import modules
    try:
        thismod = modules[modname]
    except KeyError:
        return
    these_symbols = dir(thismod)
    if paranoid:
        try:
            paranoid[:]  # sequence support
        except:
            raise ValueError('must supply a finite list for paranoid')
        else:
            these_symbols = paranoid[:]
    del modules[modname]
    for mod in modules.values():
        try:
            delattr(mod, modname)
        except AttributeError:
            pass
        if paranoid:
            for symbol in these_symbols:
                if symbol[:2] == '__':  # ignore special symbols
                    continue
                try:
                    delattr(mod, symbol)
                except AttributeError:
                    pass


class Color(object):
    RESET = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    BLINK = '\033[5m'
    NEGATIVE = '\033[7m'

    BLACK = '\033[30m'
    DARK_RED = '\033[21m'
    DARK_GREEN = '\033[32m'
    DARK_YELLOW = '\033[33m'
    DARK_BLUE = '\033[34m'
    DARK_PYRLE = '\033[35m'
    DARK_CYAN = '\033[36m'
    DARK_WHITE = '\033[37m'

    GRAY = '\033[90m'
    LIGHT_RED = '\033[91m'
    LIGHT_GREEN = '\033[32m'
    LIGHT_YELLOW = '\033[93m'
    LIGHT_BLUE = '\033[94m'
    LIGHT_PYRLE = '\033[95m'
    LIGHT_CYAN = '\033[96m'
    LIGHT_WHITE = '\033[97m'

    BACK_BLACK = '\033[40m'
    BACK_DARK_RED = '\033[41m'
    BACK_DARK_GREEN = '\033[42m'
    BACK_DARK_YELLOW = '\033[43m'
    BACK_DARK_BLUE = '\033[44m'
    BACK_DARK_PYRLE = '\033[45m'
    BACK_DARK_CYAN = '\033[46m'
    BACK_DARK_WHITE = '\033[47m'

    BACK_GRAY = '\033[100m'
    BACK_LIGHT_RED = '\033[101m'
    BACK_LIGHT_GREEN = '\033[32m'
    BACK_LIGHT_YELLOW = '\033[103m'
    BACK_LIGHT_BLUE = '\033[104m'
    BACK_LIGHT_PYRLE = '\033[105m'
    BACK_LIGHT_CYAN = '\033[106m'
    BACK_LIGHT_WHITE = '\033[107m'
