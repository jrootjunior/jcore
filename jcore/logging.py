"""
    JCore Framework. Logging module
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# FIXME: Debugging.

from jcore.utils import *


class CoreLog:
    """
    Main logger. Used in JCore.
    """

    def __init__(self, name, level, session='undefined', rewrite=False):
        """
        Construct logger.
        :param name: Logger name. used in filename and displaying.
        :param level: log-level
        :param session: unique key
        """
        self.level = level
        self.name = name
        self.session = session

        if not os.path.isdir(APP_LOCATION + '/logs'):
            os.mkdir(APP_LOCATION + '/logs')

        self.log_location = '{location}/logs/{filename}'.format(location=APP_LOCATION, filename='{name}.log'
                                                                .format(name=name))
        init_msg = '\n' + '=' * 120 + '\n\n' \
                   '\tDate: ' + time.strftime("%d.%m.%Y", time.localtime()) + '\n' \
                   '\tTime: ' + time.strftime("%H:%M:%S", time.localtime()) + '\n' \
                   '\tSession: ' + self.session + '\n\n'
        try:
            if rewrite:
                # Override file
                self._file = open(self.log_location, "w")
            else:
                # Open for appending to file
                self._file = open(self.log_location, "a", encoding='utf-8')
            self.text(init_msg)
        except:
            print(Color.DARK_RED + Color.BOLD + '::\t\tI can\'t open log file! [' + self.name + ']')

    def set_debug_level(self, level):
        self.level = level

    def _format_message(self, extra, text, type_color, text_color):
        return '{c_r}[{time}] [{c_g}{c_b}{group}{c_r}] [{c_t}{c_b}{type}{c_r}] {c_tx}{text}{c_r}' \
                   .format(time=time.strftime("%H:%M:%S", time.localtime()),
                           type=extra,
                           text=text,
                           group=self.name.upper(),
                           c_r=Color.RESET, c_b=Color.BOLD, c_t=type_color, c_tx=text_color,
                           c_g=Color.DARK_BLUE), \
               '[{date}] [{group}] [{type}] {text}' \
                   .format(date=time.strftime("%d.%m.%Y %H:%M:%S"),
                           type=extra,
                           group=self.name.upper(),
                           text=text)

    def info(self, message, file=True, display=True):
        msg_1, msg_2 = self._format_message('INFO', message, Color.LIGHT_GREEN, Color.RESET)
        if display:
            print(msg_1)
        if file:
            self.text(msg_2)

    def system(self, message, file=True, display=True):
        msg_1, msg_2 = self._format_message('SYSTEM', message, Color.LIGHT_BLUE, Color.RESET)
        if display:
            print(msg_1)
        if file:
            self.text(msg_2)

    def warning(self, message, file=True, display=True):
        msg_1, msg_2 = self._format_message('WARNING', message, Color.LIGHT_YELLOW, Color.RESET)
        if display:
            print(msg_1)
        if file:
            self.text(msg_2)

    def error(self, message, file=True, display=True):
        msg_1, msg_2 = self._format_message('ERROR', message, Color.LIGHT_RED, Color.RESET)
        if display:
            print(msg_1)
        if file:
            self.text(msg_2)

    def text(self, message):
        self._file.write(message + '\n')
        self._file.flush()

    def debug(self, message, file=True, display=True):
        if self.level == logging.DEBUG:
            msg_1, msg_2 = \
                self._format_message('DEBUG', message, Color.BACK_DARK_CYAN + Color.LIGHT_YELLOW, Color.LIGHT_PYRLE)
            if display:
                print(msg_1)
            if file:
                self.text(msg_2)


