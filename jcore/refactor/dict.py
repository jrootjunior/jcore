def gen_int(start, finish):
    result = ''
    for item in range(start, finish + 1):
        result += chr(item)
    return result


def gen_chr(start, finish):
    return gen_int(ord(start), ord(finish))


print('Alphabet lower:\t\t', gen_chr('a', 'z'))
print('Alphabet upper:\t\t', gen_chr('A', 'Z'))
print('Symbols:\t\t\t', gen_int(33, 47) + gen_int(58, 64) + gen_int(91, 96) + gen_int(123, 126))
