"""
    JCore Framework. Config module
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jcore.logging import CoreLog
from jcore.utils import *


class Config(object):
    """
    Storage object
    """
    def __init__(self, name, default=None, auto_load=False, logger=None):
        """
        Init storage
        :param name: config name. Used for filename
        :param default: default data
        :param auto_load: automatic load from file
        :param logger: logger for this storage
        """

        if not default:
            default = {}

        if not logger:
            self.logger = CoreLog('cfg-' + name, logging.DEBUG)
        else:
            self.logger = logger

        self.data = default
        self.name = name
        self.default = default
        self.auto_load = auto_load
        self.filename = name + '.json'
        self.file_location = APP_LOCATION + '/data/'

        # If need to load config.
        if auto_load:
            self.load()

    def load(self):
        """
        Load data from storage
        :return: bool
        """
        # If 'data' dir not exist
        if not os.path.isdir(self.file_location):
            self.logger.warning('Configs path is not found!')
            os.mkdir(APP_LOCATION + '/data')
            return self.load()

        # If config is not exist
        if not os.path.exists(self.file_location + self.filename):
            self.logger.warning('Config file "' + self.filename + '" not found! Generate new..')
            self.data = self.default
            return self.save()

        # Load file and check it
        with open(self.file_location + self.filename, 'r', encoding='utf8') as file:
            self.data = valid_json(file.read())
            if self.data:
                # Success loading config
                self.logger.debug('Config file "' + self.filename + '" is loaded.')
                return True
            # If json file not reachable
            self.logger.warning('Missing to load config file "' + self.filename + '".')
            self.data = self.default
            self.save()
            return False

    def save(self):
        """
        Save all data to storage
        :return:
        """
        # If 'data' dir exist
        if not os.path.isdir(self.file_location):
            # If dir not exist
            self.logger.warning('Configs path is not found!')
            # Make directory
            os.mkdir(APP_LOCATION + '/data')
            return self.save()

        # Load file and check it
        with open(self.file_location + self.filename, 'w', encoding='utf8') as file:
            json.dump(self.data, file, separators=(', ', ': '), indent=4, ensure_ascii=False)
            self.logger.debug('Config file "' + self.filename + '" is saved.')
            return True

    def backup(self):
        """
        Make backup
        :return: filename
        """
        import shutil
        if not os.path.exists(APP_LOCATION + '/backup'):
            os.mkdir(APP_LOCATION + '/backup')
        new_file = '/backup/' + self.name + '_' + \
                   time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime()) + '.json'
        shutil.copy(self.file_location + self.filename, APP_LOCATION + new_file)
        return new_file

    def restore(self, key):
        """
        Restore backup
        :return:
        """
        # TODO: Make restoring function
        pass

    def set_param(self, key, value, auto_save=False):
        """
        Set value
        :param key:
        :param value:
        :param auto_save:
        """
        self.data[key] = value
        if auto_save:
            self.save()

    def get_param(self, key, default=None, fix=False):
        """
        Get value from storage
        :param key: needed key.
        :param default: default value.
        :param fix: add key to storage if not exist.
        :return: value from storage or default.
        """
        if key in self.data:
            return self.data[key]
        elif fix:
            self.set_param(key, default)
            if self.auto_load:
                self.save()
        return default

    def get_data(self):
        return self.data

    def get_string_data(self, formated=False):
        """
        Get formatted to string data.
        :param formated: need human readable style
        :return: string
        """
        if formated:
            return json.dumps(self.data, separators=(', ', ': '), indent=4, ensure_ascii=False)
        else:
            return json.dumps(self.data, ensure_ascii=False)
