"""
    JCore Framework. Fun shaker
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time

from jcore.watchdog import WSignal


class FunShaker:
    """
    Main action manager
    """
    queue = []

    def __init__(self, jcore, thread=False):
        self.jcore = jcore
        self.timeout = jcore.config.data['fun']['timeout']
        self.threader = thread

        self.jcore.log_fun.info('Init FunShaker.')
        if thread:
            self.jcore.log_fun.info('Init FunShaker. Timeout: ' + str(self.timeout) + 's.')
            self.jcore.watchdog.add_watch(self._process, 'FunShaker', True)

        self.index = 0

    def _process(self):
        """
        Fun processor. Main loop.
        :return:
        """

        def routine(title, signal):
            # TODO: Just do it!!!
            return True

        try:
            self.jcore.watchdog.loop('FunShaker', routine, self.timeout)
        except KeyboardInterrupt:
            self.jcore.watchdog.signal('KeyboardInterrupt', WSignal.SIGSTOP)

    def fun(self, name, function, callback, *args, **kwargs):
        """
        Register event.
        :param name: title
        :param function: init function
        :param callback: function callback
        :param args:
        :param kwargs:
        """
        record = {
            'id': self.index,
            'name': name,
            'function': function,
            'callback': callback,
            'time': time.time(),
            'data': None
        }
        self.queue.append(record)
        if function:
            function(self.index, *args, **kwargs)
        self.jcore.log_fun.debug('Add "' + name + '" [' + str(self.index) +
                                 ']. Total in queue ' + str(len(self.queue)) + '.')
        self.index += 1

    def call_event(self, key, data):
        """
        Finish event by ID
        :param key: target ID
        :param data:
        """
        for item in self.queue:
            if key == self.queue[self.queue.index(item)]['id']:
                func = self.queue[self.queue.index(item)]['callback']
                self.jcore.log_fun.debug('Call "' + self.queue[self.queue.index(item)]['name'] +
                                         '" [' + str(key) + ']. Total in queue ' + str(len(self.queue) - 1) + '.')
                if func:
                    # BugFix: TypeError: 'NoneType' object is not callable
                    func(data)
                self.queue.pop(self.queue.index(item))

    def del_event(self, key):
        """
        Remove action.
        :param key: target ID
        :return:
        """
        for item in self.queue:
            if key == self.queue[self.queue.index(item)]['id']:
                self.queue.pop(self.queue.index(item))
                self.jcore.log_fun.debug('Remove "' + self.queue[self.queue.index(item)]['name'] +
                                         '" [' + str(key) + ']. Total in queue ' + str(len(self.queue)) + '.')
