"""
    JCore Framework. WatchDog module
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jcore.utils import *


class WatchDog:
    def __init__(self, jcore):
        # Global signals
        self.jcore = jcore
        self.signals = []
        # Watchdog preset list
        self.watchlist = {}
        self.add_watch(self._process, 'WatchDog', True)
        self.jcore.log.info('Init WatchDog.')

    def add_watch(self, function, title, enable=True):
        if title not in self.watchlist:
            try:
                self.watchlist[title] = {'function': function,
                                         'thread': threading.Thread(target=function, name=title),
                                         'enable': enable}
                self._standby(title)
            except:
                pass

    def del_watch(self, title):
        if title in self.watchlist:
            self.watchlist.pop(title)
            return True
        else:
            return False

    def get_thr_names(self):
        thr = []
        for item in threading.enumerate():
            thr.append(item.getName())
        return thr

    def _standby(self, item):
        if item in self.watchlist:
            try:
                process = threading.Thread(target=self.watchlist[item]['function'], name=item)
                process.start()
            except:
                return False
            self.watchlist[item]['thread'] = process

    def signal(self, title, signal, alive=60):
        self.signals.append(
            {
                'title': title,
                'signal': signal,
                'listened': [],
                'time': time.time(),
                'alive': alive
            }
        )

    def _process(self):
        """
        WatchDog. Main loop.
        :return:
        """
        def routine(title, signals):
            proc_list = self.get_thr_names()
            for item in self.watchlist:
                if item not in proc_list:
                    self._standby(item)
            return True
        try:
            self.loop('WatchDog', routine, 10)
        except KeyboardInterrupt:
            self.signal('KeyboardInterrupt', WSignal.SIGSTOP)

    def loop(self, title, function, timeout=60):
        proc = True
        self.jcore.log.info('Started ' + title + ' process.')
        while proc:
            active_sig = []
            for signal in self.signals:
                if title not in self.signals[self.signals.index(signal)]['listened']:
                    active_sig.append(self.signals[self.signals.index(signal)]['signal'])
                    self.signals[self.signals.index(signal)]['listened'].append(title)
            if function(title, active_sig):
                if not self.timeout(timeout, active_sig):
                    proc = not proc
                continue
            else:
                proc = not proc
        self.jcore.log.warning('Process ' + title + ' is stopped.')

    def timeout(self, period, title=None):
        for i in range(period):
            signals = self.get_signals(title, mark=True)
            if WSignal.SIGKILL in signals or WSignal.SIGSTOP in signals:
                return False
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                return False
        return True

    def get_signals(self, title=None, mark=True):
        result = []
        if title:
            for signal in self.signals:
                if title not in self.signals[self.signals.index(signal)]['listened']:
                    result.append(self.signals[self.signals.index(signal)]['signal'])
                    if mark:
                        self.signals[self.signals.index(signal)]['listened'].append(title)
        else:
            for signal in self.signals:
                result.append(self.signals[self.signals.index(signal)]['signal'])
        return result


class WSignal(object):
    SIGHUP = 1
    SIGKILL = 9
    SIGSTOP = 23

