"""
    JCore Framework. Plugins manager module
    Copyright (C) Alex Root Junior <jroot.junior@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# TODO: REWRITE THIS BULL SHIT!
# TODO: Плагины упаковывать в пакеты для улучшения поддержки многомодульных плагинов.
# FIXME: Исправить загрузку/выгрузку.

from jcore.config import Config
from jcore.utils import *


class PluginsManager:
    # Hide signals from output
    marked = []
    mode = False  # True = show marked

    def __init__(self, jcore, plugin_core):
        self.jcore = jcore
        self.log = jcore.log_plugins
        self.plugins_config = Config('plugins', {'disabled': []}, auto_load=True, logger=self.jcore.log_loader)
        self.imported_modules = []
        self.plugins = []
        self.plugin_core = plugin_core

        # Check plugins dir
        if not os.path.isdir(PLUGINS_LOCATION):
            os.mkdir(PLUGINS_LOCATION)

        # Add plugins dir to PATH
        sys.path.insert(0, PLUGINS_LOCATION)

        self.log.info('Inited Plugins core.')

    def mark_signal(self, signal_name, status=True):
        if status:
            if signal_name not in self.marked:
                self.marked.append(signal_name)
        else:
            if signal_name in self.marked:
                self.marked.pop(self.marked.index(signal_name))

    def set_signal_mode(self, status):
        self.mode = not status

    def load_plugins(self):
        self.log.debug('Scan plugins dir.')
        list = os.listdir(PLUGINS_LOCATION)
        for item in list:
            if item.startswith('_'):
                continue
            try:
                __import__(os.path.splitext(item)[0], None, None, [''])
                self.imported_modules.append(os.path.splitext(item)[0])
            except ImportError:
                self.log.error('Plugin module "{file}" is wrong!'.format(file=item))
        self.log.debug('Imported {count} modules.'.format(count=len(list)))

        self.log.debug('Create plugins instances.')
        for plugin in self.plugin_core.__subclasses__():
            try:
                p = plugin(self.jcore)
            except:
                self.log.error('Plugin {title} is missing!'.format(title=plugin.__mame__))
            else:
                # self.imported_modules.pop(self.imported_modules.index(plugin.__module__))
                self.plugins.append(p)
                self.log.system('Plugin "' + Color.LIGHT_YELLOW + plugin.Name + Color.RESET + '" is loaded.')

        self.log.info('Loaded {count} plugins.'.format(count=len(self.plugins)))

        # Clear unused imports. (If found wrong plugins file)
        # for item in self.imported_modules:
        #     delete_module(item)
        return

    def unload_plugins(self):
        self.log.system('Unload plugins..')
        for plugin in self.plugins:
            name = plugin.Name
            module = plugin.__module__
            if plugin.Enabled:
                plugin.on_unload()
            for k in [x for x in sys.modules.keys() if x.startswith(module + '.')]:
                del sys.modules[k]
            del sys.modules[module]
            try:
                delete_module(module)
                os.unlink(PLUGINS_LOCATION + '/' + module + '.pyc')
            except KeyError:
                pass
            except FileNotFoundError:
                pass
            del self.plugins[self.plugins.index(plugin)]
            del plugin
            self.log.system('Plugin "' + Color.LIGHT_YELLOW + name + Color.RESET + '" is unloaded.')
        self.imported_modules = []
        return

    def init_plugins(self):
        # Sort
        self.plugins = sorted(self.plugins, key=lambda plugin: plugin.Priority)

        for plugin in self.plugins:
            if not plugin.Enabled \
                    and plugin.Name not in self.plugins_config.data['disabled'] \
                    and plugin.Priority != PlPriority.LIBRARY:
                plugin.on_load()
                self.log.info('Loaded: ' + plugin.get_info() + '.')
            elif plugin.Priority == PlPriority.LIBRARY:
                self.log.info('Loaded library: ' + plugin.get_info() + '.')

    def restart(self):
        self.unload_plugins()
        self.load_plugins()
        self.init_plugins()

    def call_signal(self, command, data=None):
        if self.mode and command in self.marked:
            self.log.info('Call signal to plugins: (' + str(command) + ', ' + str(data) + ')')
        elif not self.mode and not command in self.marked:
            self.log.info('Call signal to plugins: (' + str(command) + ', ' + str(data) + ')')
        result = False
        for plugin in self.plugins:
            try:
                if plugin.Enabled:
                    result = plugin.on_signal(command, data)
            except:
                self.log.error('Error while occurred calling signal in "' + plugin.Name + '" plugin.')
            if result:
                return True
        return False


# Base plugin class
class Plugin(object):
    Name = 'None'
    Description = 'None'
    Version = '0.1a'
    Requirements = []
    Priority = 100
    Enabled = False
    jcore = None

    def __init__(self, jcore):
        self.Enabled = False
        self.data = {}
        self.commands = []
        self.events = {}
        self.jcore = jcore

        self.del_tel_command = self.jcore.telnet.del_command

    # Event: on_load plugin. Need override
    def on_load(self):
        self.Enabled = True

    # Event: on_load plugin. Need override optionally
    def on_unload(self):
        self.Enabled = False
        return True

    def on_signal(self, command, data):
        """
        :return: sent True if need stop calling this method in other plugins
        """
        if command in self.events:
            return self.events[command](data)
        return False

    def add_event(self, event='', callback=None):
        """
        Register event called in "on_signal"
        :param event: event name
        :param callback: called method
        """
        self.events[event] = callback

    def remove_event(self, event):
        """
        Unregister event
        :param event: name
        :return: (bool) status
        """
        if event in self.events:
            self.events.pop(event)
            return True
        return False

    def add_tel_command(self, command, function, description):
        self.commands.append(self.jcore.telnet.add_command(command, function, description))

    # Get formatted information about plugin
    def get_info(self):
        return '{name} (v{version}): {description}' \
            .format(name=self.Name, version=self.Version, description=self.Description)


class PlPriority(object):
    LIBRARY = 0

    CORE = 1
    CORE_MOD = 5
    CORE_MOD_1 = 6
    CORE_MOD_2 = 7
    CORE_PLUGIN = 8
    CORE_PLUGIN_1 = 9
    CORE_PLUGIN_2 = 10

    SYSTEM = 20
    SYSTEM_MOD = 21
    SYSTEM_MOD_1 = 22
    SYSTEM_MOD_2 = 23
    SYSTEM_PLUGIN = 24
    SYSTEM_PLUGIN_1 = 25
    SYSTEM_PLUGIN_2 = 26

    MANAGER = 30
    MANAGER_1 = 31
    MANAGER_2 = 32

    MAIN_ROUTINE = 50

    PLUGIN = 60
    PLUGIN_1 = 61
    PLUGIN_2 = 62
    PLUGIN_3 = 63
    PLUGIN_4 = 64
    PLUGIN_5 = 65
    PLUGIN_6 = 66
    PLUGIN_7 = 67
    PLUGIN_8 = 68
    PLUGIN_9 = 69

    DAEMON = 90

    BACKGROUND = 100
